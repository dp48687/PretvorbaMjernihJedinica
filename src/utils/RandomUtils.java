package utils;

import java.util.Random;

public class RandomUtils {
    private static Random random = new Random();

    public static int randomInteger(int lowerBound, int upperBound) {
        return (int) (lowerBound + (upperBound - lowerBound) * randomDouble());
    }

    public static double randomDouble() {
        return random.nextDouble();
    }

    public static boolean randomBoolean() {
        return random.nextBoolean();
    }

}
