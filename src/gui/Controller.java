package gui;

import fileservice.FileService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mjernejedinice.Converter;
import pdfcreator.PDFTestCreator;
import test.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private int questions;
    private String testName, clasName, teacherName;
    private Date currentDate;
    private String[] students;

    @FXML
    private VBox root;

    @FXML
    private DatePicker date;

    @FXML
    private CheckBox generateRes;

    @FXML
    private TextField questionQuantity, name, teacher, className;

    @FXML
    private Button configBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        date.setOnAction(t ->
            currentDate = Date.from(
                    date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()
            )
        );
    }

    @FXML
    public void generate() {
        try {
            questions = Integer.parseInt(questionQuantity.getText());
        } catch (NumberFormatException ex) {
            String errorMessage = questionQuantity.getText().isEmpty() ?
                    "Unesite broj pitanja u generiranom testu!" :
                    "Broj pitanja treba biti prirodni broj.";
            launchErrorDialog(errorMessage);
            return;
        }
        testName = !name.getText().equals("") ? name.getText() : " ";
        teacherName = !teacher.getText().equals("") ? teacher.getText() : " ";
        clasName = !className.getText().equals("") ? className.getText() : " ";

        Test[] tests = new Test[students.length];
        for (int i = 0; i < students.length; ++i) {
            tests[i] = Converter.generateTest(
                    questions,
                    currentDate,
                    clasName,
                    " ",
                    teacherName,
                    students[i]
            );
            tests[i].setTestName(testName);
        }
        startGenerating(tests);
    }

    @FXML
    public void launchConfig() {
        try {
            File chosen = FileService.selectFile(
                    (Stage) (root.getScene().getWindow())
            );
            configBtn.setText(chosen.getAbsolutePath());
            students = new String(
                    Files.readAllBytes(chosen.toPath()),
                    StandardCharsets.UTF_8
            ).trim().split("\n");
        } catch (IOException e) {
            launchErrorDialog("Greška kod učitavanja datoteke.");
        } catch (NullPointerException e) {
            launchErrorDialog("Trebate zadati datoteku sa popisom imena\nučenika/ca!");
        }
    }

    private void launchErrorDialog(String errorMessage) {
        Stage userInputStage = new Stage();
        VBox box = (VBox) getRoot(false);
        ((Label) (box.getChildren().get(0))).setText(errorMessage);
        box.getChildren().get(1).setOnMouseClicked(
                event -> userInputStage.close()
        );
        userInputStage.setScene(new Scene(box));
        userInputStage.show();
    }

    private Node getRoot(boolean isMainScene) {
        try {
            String path = isMainScene ? "/gui/prompt.fxml" : "/gui/errorDialog.fxml";
            return FXMLLoader.load(Launcher.class.getResource(path));
        } catch (IOException e) {
            return null;
        }
    }

    private void startGenerating(Test[] tests) {
        final String path = FileService.selectFolder(
                (Stage) (root.getScene().getWindow())
        ).getAbsolutePath();

        new Thread(() ->
                PDFTestCreator.generate(
                        tests,
                        path,
                        generateRes.isSelected(),
                        false
                )
        ).start();
    }

}
