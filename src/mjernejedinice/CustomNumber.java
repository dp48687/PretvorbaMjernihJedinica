package mjernejedinice;

import utils.RandomUtils;

public class CustomNumber {
    private static StringBuilder numberBuilder = new StringBuilder();
    private static final int NUM_DECIMAL_PLACES = 10;

    private String mainPart;
    private int potention;

    public CustomNumber(String mainPart, int potention) {
        this.mainPart = mainPart;
        this.potention = potention;
    }

    public static CustomNumber generate(boolean allowNegativeValues,
                                        int minPotention,
                                        int maxPotention,
                                        double minValue,
                                        double maxValue) {
        numberBuilder.delete(0, numberBuilder.length());
        int selectedDecPlace = RandomUtils.randomInteger(
                -NUM_DECIMAL_PLACES / 2 - 1,
                NUM_DECIMAL_PLACES / 2
        );

        int i = 0;
        if (selectedDecPlace < 0) {
            for (; i < -selectedDecPlace; ++i) {
                numberBuilder.append('0');
                if (i == 0) {
                    numberBuilder.append('.');
                }
            }
            while (i++ < NUM_DECIMAL_PLACES) {
                numberBuilder.append(RandomUtils.randomInteger(1, 9));
            }
        } else {
            for (; i <= selectedDecPlace; ++i) {
                numberBuilder.append(RandomUtils.randomInteger(1, 9));
            }
            if (i < NUM_DECIMAL_PLACES) {
                numberBuilder.append('.');
                while (i++ < NUM_DECIMAL_PLACES - 1) {
                    numberBuilder.append(RandomUtils.randomInteger(0, 9));
                }
            }
        }

        if (allowNegativeValues && RandomUtils.randomBoolean()) {
            numberBuilder.insert(0, '-');
        }

        int maxIterations = 30;
        int potention = 0;
        double mainPart = Double.parseDouble(numberBuilder.toString());
        while (--maxIterations >= 0) {

            potention = RandomUtils.randomInteger(minPotention, maxPotention);
            double wholeValue = mainPart * Math.pow(10, potention);

            if (wholeValue > minValue && wholeValue < maxValue) {
                break;
            }

        }

        CustomNumber customNumber = new CustomNumber(numberBuilder.toString(), potention);
        customNumber.shortenTrailingZeros();
        customNumber.arrangeZeros();
        customNumber.cutFarDecimals();

        return customNumber;
    }

    private void shortenTrailingZeros() {
        while (numberBuilder.charAt(numberBuilder.length() - 1) == '0') {
            numberBuilder.delete(numberBuilder.length() - 1, numberBuilder.length());
        }
    }

    public void arrangeZeros() {
        int firstNonZeroIndex = firstNonZeroIndex();
        if (firstNonZeroIndex > 4) {
            numberBuilder.delete(0, numberBuilder.length());
            numberBuilder.append(mainPart);
            numberBuilder.delete(firstNonZeroIndex + 1, numberBuilder.length());
            mainPart = numberBuilder.toString();
        }
    }

    private int firstNonZeroIndex() {
        int i = 0;
        for (; i < mainPart.length(); ++i) {
            switch (mainPart.charAt(i)) {
                case '-':
                case '.':
                case '0':
                    break;
                default:
                    return i;
            }
        }
        return i;
    }

    private int decimalPointIndex() {
        return mainPart.lastIndexOf('.');
    }

    private void cutFarDecimals() {
        boolean isLowerThanAbs1 = mainPart.charAt(0) == '0' ||
                                  mainPart.charAt(0) == '-' && mainPart.charAt(1) == '0';
        if (!isLowerThanAbs1) {
            mainPart = mainPart.substring(0, decimalPointIndex() + 3);
        } else {
            for (int i = decimalPointIndex() + 1; i < mainPart.length(); ++i) {
                if (mainPart.charAt(i) != '0') {
                    mainPart = mainPart.substring(0, Math.min(i + 2, mainPart.length()));
                    return;
                }
            }
        }
    }

    public void setPotention(int potention) {
        this.potention = potention;
    }

    public String getMainPart() {
        return mainPart;
    }

    @Override
    public String toString() {
        String[] mainParts = String.format("%1.3e", Double.parseDouble(mainPart)).split("e");

        while (mainParts[0].charAt(mainParts[0].length() - 1) == '0') {
            mainParts[0] = mainParts[0].substring(0, mainParts[0].length() - 1);
        }
        if (mainParts[0].charAt(mainParts[0].length() - 1) == '.') {
            mainParts[0] = mainParts[0].substring(0, mainParts[0].length() - 1);
        }

        return mainParts[0] + " x 10^" + (int) (Double.parseDouble(mainParts[1]) + potention);
    }

}
