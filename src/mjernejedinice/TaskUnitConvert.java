package mjernejedinice;

import test.general.TextFillTask;

public class TaskUnitConvert extends TextFillTask {

    public TaskUnitConvert(String question, String[] correct) {
        super(question, correct, 2, 0, "units");
    }

    public static TaskUnitConvert generateRandom(int quantity) {
        Converter c = new Converter();

        Object[] randomTasks = c.generateRandomTasks(quantity);

        return new TaskUnitConvert(randomTasks[0].toString(), randomTasks[1].toString().split("\n"));
    }

}
