package mjernejedinice;

import test.Test;
import test.general.Task;
import utils.RandomUtils;

import java.util.*;

public class Converter {

    private Map<String, char[]> unitPrefixMapping = new HashMap<>();

    private static Map<String, String> taskMapping = new HashMap<>();

    {
        unitPrefixMapping.put("m", new char[]{'p', 'n', '\u03bc', 'm', 'c', 'd', ' ', 'k'});
        unitPrefixMapping.put("m^2", new char[]{'p', 'n', '\u03bc', 'm', 'c', 'd', ' ', 'k'});
        unitPrefixMapping.put("m^3", new char[]{'p', 'n', '\u03bc', 'm', 'c', 'd', ' ', 'k'});
        unitPrefixMapping.put("g", new char[]{'\u03bc', 'm', ' ', 'k'});
        unitPrefixMapping.put("C", new char[]{'p', 'n', '\u03bc', 'm', ' '});
        unitPrefixMapping.put("F", new char[]{'p', 'n', '\u03bc', 'm', ' '});
        unitPrefixMapping.put("A", new char[]{'p', 'n', '\u03bc', 'm', ' ', 'k'});
        unitPrefixMapping.put("V", new char[]{'p', 'n', '\u03bc', 'm', ' ', 'k', 'M'});
        unitPrefixMapping.put("W", new char[]{'\u03bc', 'm', ' ', 'k', 'M', 'G'});
    }

    private double getPrefixValue(char prefix) {
        switch (prefix) {
            case 'p':
                return 1E-12;
            case 'n':
                return 1E-9;
            case '\u03bc':
                return 1E-6;
            case 'm':
                return 1E-3;
            case 'c':
                return 1E-2;
            case 'd':
                return 1E-1;
            case ' ':
                return 1;
            case 'k':
                return 1E3;
            case 'M':
                return 1E6;
            default:
                return 1E9;
        }
    }

    public Object[] generateRandomTask() {
        double randomFloat = RandomUtils.randomDouble();

        String unit;

        if (randomFloat < 0.4) {
            unit = RandomUtils.randomBoolean() ? "m^2" : "m^3";
        } else if (randomFloat < 0.5) {
            unit = "m";
        } else if (randomFloat < 0.6) {
            unit = "g";
        } else if (randomFloat < 0.7) {
            unit = "A";
        } else if (randomFloat < 0.77) {
            unit = "C";
        } else if (randomFloat < 0.87) {
            unit = "F";
        } else if (randomFloat < 0.935) {
            unit = "W";
        } else {
            unit = "V";
        }

        char firstPrefix, secondPrefix;
        char[] possiblePrefixes = unitPrefixMapping.get(unit);

        do {
            firstPrefix = possiblePrefixes[RandomUtils.randomInteger(0, possiblePrefixes.length - 1)];
            secondPrefix = possiblePrefixes[RandomUtils.randomInteger(0, possiblePrefixes.length - 1)];
        } while (firstPrefix == secondPrefix);

        CustomNumber randomNumber = CustomNumber.generate(
                false, -5, 5,1E-9, 1E6
        );
        randomNumber.setPotention(0);

        String randomValue = randomNumber.getMainPart();

        randomNumber.setPotention((int) Math.log10(getDifference(firstPrefix, secondPrefix, unit)));
        String convertedValue = randomNumber.toString();

        StringBuilder taskBuilder = new StringBuilder();
        taskBuilder.append(randomValue);
        taskBuilder.append(" ");
        if (firstPrefix != ' ') {
            taskBuilder.append(firstPrefix);
        }
        taskBuilder.append(unit);
        taskBuilder.append(" = ______________  ");
        if (secondPrefix != ' ') {
            taskBuilder.append(secondPrefix);
        }
        taskBuilder.append(unit);

        return new Object[]{taskBuilder.toString(), convertedValue};
    }

    public Object[] generateRandomTasks(int quantity) {
        StringBuilder resultsBuilder = new StringBuilder();

        String longest = "";
        String[] tasks = new String[quantity];

        List<String> resultsList = new ArrayList<>(quantity);

        for (int i = 0; i < quantity; ++i) {
            Object[] randomTask = generateRandomTask();
            String task = randomTask[0].toString();
            if (task.split("=")[0].length() >= longest.length()) {
                longest = task.split("=")[0];
            }
            tasks[i] = randomTask[0].toString();
            resultsBuilder.append(randomTask[1]);
            resultsBuilder.append('\n');
            resultsList.add(randomTask[1].toString() + '\n');
        }

        int toAppend = longest.length();
        StringBuilder testBuilder = new StringBuilder();
        for (int i = 0; i < tasks.length; ++i) {
            String[] chunks = tasks[i].split("=");
            String spacesToAppend = "";
            for (int j = 0; j < toAppend - chunks[0].length(); ++j) {
                spacesToAppend += " ";
            }
            String built = "   " + chunks[0] + spacesToAppend + " = " + spacesToAppend + chunks[1] + '\n';
            testBuilder.append(built);

            if (taskMapping.size() < tasks.length) {
                taskMapping.put(built, resultsList.get(i));
            }
        }

        appendUnitWords(testBuilder);

        return new Object[] {
                testBuilder.toString(),
                resultsBuilder.toString()
        };
    }

    public static Test generateTest(int quantity,
                                    Date date,
                                    String className,
                                    String group,
                                    String professor,
                                    String student) {
        List<Task> tasks = new ArrayList<>(1);
        tasks.add(TaskUnitConvert.generateRandom(quantity));
        return new Test(date, className, group, professor, student, "TEST", tasks);
    }

    private static void appendUnitWords(StringBuilder testBuilder) {
        testBuilder.append("\n\n         m - metar          ");
        testBuilder.append("\n         g - gram           ");
        testBuilder.append("\n         C - kulon          ");
        testBuilder.append("\n         F - farad          ");
        testBuilder.append("\n         V - volt           ");
        testBuilder.append("\n         A - amper          ");
        testBuilder.append("\n         W - vat            \n");
    }

    private double getDifference(char first, char second, String unit) {
        double ratio = getPrefixValue(first) / getPrefixValue(second);

        if (unit.endsWith("2")) {
            return ratio * ratio;
        } else if (unit.endsWith("3")) {
            return ratio * ratio * ratio;
        } else if (unit.endsWith("4")) {
            return ratio * ratio * ratio * ratio;
        }

        return ratio;
    }

}