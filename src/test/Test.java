package test;

import test.general.Task;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;

public class Test {
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MM. yyyy.");
    private static Date date = new Date();
    private static String warning =
            "Zabranjeno je prepisivanje, dogovaranje, korištenje mobilnih uređaja, šalabahtera, i sličnih pomagala.\n" +
            "Nepridržavanje ovog pravila povlači negativnu ocjenu.\n\nSretno u rješavanju ispita!\n\n";

    private String className = " ";
    private String group = " ";
    private String professor = " ";
    private String student = " ";
    private String id = " ";
    private String testName = "TEST";
    private int maxPoints = 1;
    private Object results;
    private List<Task> tasks;

    public Test(Date date,
                String className,
                String group,
                String professor,
                String student,
                String testName,
                List<Task> tasks) {
        if (date != null) {
            this.date = date;
        }
        this.className = className;
        this.group = group;
        this.professor = professor;
        this.student = student;
        this.tasks = tasks;
        this.testName = testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public Test(List<Task> tasks) {
        this.tasks = tasks;
    }

    private List<String> buildResults() {
        List<String> results = new ArrayList<>(tasks.size());

        tasks.forEach(t -> results.add(t.getAttribute("steps").toString()));

        return results;
    }

    public void forEachTask(Consumer<Task> action) {
        tasks.forEach(action);
    }

    public void permutateQuestions() {
        Collections.shuffle(tasks, new Random());
    }

    public Object getAttribute(String id) {
        switch (id) {
            case "id":
                return this.id;
            case "date":
                return dateFormatter.format(date);
            case "class":
                return className;
            case "group":
                return group;
            case "professor":
                return professor;
            case "student":
                return student;
            case "points":
                return maxPoints;
            case "warning":
                return warning;
            case "testName":
                return testName;
            case "results":
                return buildResults();
            default:
                throw new IllegalArgumentException(
                        "Unknown attribute: '" + id + "'"
                );
        }
    }

}
