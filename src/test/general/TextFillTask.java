package test.general;

public abstract class TextFillTask extends Task {

    protected String[] correct;

    public TextFillTask(String question,
                              String[] correct,
                              int pointsCorrect,
                              int pointsWrong,
                              String tag) {
        super(question, null, pointsCorrect, pointsWrong, tag);
        this.correct = correct;
        this.answer = correct;
        this.steps = buildSteps(correct);
    }

    private String buildSteps(String[] results) {
        StringBuilder rBuilder = new StringBuilder();

        for (String result : results) {
            rBuilder.append(result);
            rBuilder.append('\n');
        }
        rBuilder.deleteCharAt(rBuilder.length() - 1);

        return rBuilder.toString();
    }

    @Override
    protected Object makeAnswer() {
        return correct;
    }

}
